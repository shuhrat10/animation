//
//  FlowLayout.swift
//  Animation
//
//  Created by Shukhrat Tursunov on 11/10/16.
//  Copyright © 2016 Home. All rights reserved.
//

import UIKit

class FlowLayout: UICollectionViewFlowLayout {

  override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
    return true
  }
  
  override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
    
    var offsetAdjustment = MAXFLOAT;
    let horizontalCenter = proposedContentOffset.x + (collectionView!.bounds.width / 2.0);
    let targetRect = CGRect(x: proposedContentOffset.x, y: 0.0, width: collectionView!.bounds.size.width, height: collectionView!.bounds.size.height)
    
    
    let array = super.layoutAttributesForElements(in: targetRect)
    for layoutAttributes in array! {
      
      let itemHorizontalCenter = layoutAttributes.center.x
      if (fabs(itemHorizontalCenter - horizontalCenter) < fabs(CGFloat(offsetAdjustment))) {
        offsetAdjustment = Float(itemHorizontalCenter) - Float(horizontalCenter)
      }
    }
    
    
    return CGPoint(x: proposedContentOffset.x + CGFloat(offsetAdjustment), y: proposedContentOffset.y)
  }
  
  override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
    
    let attributesAry = super.layoutAttributesForElements(in: rect)!
    
    let visAbleRect = CGRect(x: collectionView!.contentOffset.x, y: collectionView!.contentOffset.y,width: collectionView!.frame.size.width, height: collectionView!.frame.size.height)
    let midX = visAbleRect.midX
    
    print(visAbleRect)
    
    for eachAttributes in attributesAry {
      
      let distance = eachAttributes.center.x - midX
      let scale = distance / itemSize.width
      let scaleY = 1 - fabs(scale) * 0.2
      
      eachAttributes.transform3D = CATransform3DMakeScale(1.0, scaleY, scaleY)
    }
    
    return attributesAry
  }
  
}
