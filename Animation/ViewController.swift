//
//  ViewController.swift
//  Animation
//
//  Created by Shukhrat Tursunov on 11/10/16.
//  Copyright © 2016 Home. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

  override func viewDidLoad() {
    super.viewDidLoad()

  }

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return 10
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
    
    return cell
  }

}
